## Tests effectués avec le battletag "cats#11481" 

## Peut etre lancé en serveur local pour test en récupérant le repo: "https://gitlab.com/F31G/profile-overwatch"

## Site: http://aloof-prof-overwatch.surge.sh/


## Contexte du projet

La doc de l'API qu'on va utiliser : https://ow-api.com/docs/#introduction


* Créer un formulaire permettant de saisir date de naissance, nom, prénom et battletag.

* Grâce à JS, vérifiez dès qu'on change d'input que le champ est conforme à ce qui est attendu.

* En particulier, ajoutez un validateur custom pour vérifier le format du battletag.

* Directement dans l'input, remplacer # par -.

* Si le battletag est au bon format, utilisez l'API indiquée plus haut (via AJAX) pour récupérer les infos du profil et affichez les infos suivantes directement le DOM grâce à JS :


    Parties jouées
    Classement
    Nombre de défaites
    Nombre de victoires

* Attention, interdiction d'utiliser jQuery!

Pour AJAX et les formulaires, MDN est (plus que jamais) votre ami.

